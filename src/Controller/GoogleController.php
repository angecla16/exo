<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Inscription;
use App\Form\InscriptionType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;

class GoogleController extends AbstractController
{
    /**
     *
     */
    public function index()
    {
        return $this->render('google/index.html.twig', [
            'controller_name' => 'GoogleController',
        ]);
    }

    public function new(Request $request)
    {
        // just setup a fresh $inscription object (remove the example data)
        $inscription = new Inscription();
    
        $form = $this->createForm(InscriptionType::class, $inscription);
    
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $inscription = $form->getData();
    
            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($inscription);
            $entityManager->flush();
    
            return $this->redirectToRoute('show');
        }
    
        return $this->render('google/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function showInscrits(){
    
        $listeInscrits = $this->getDoctrine()
        ->getManager()
        ->getRepository(Inscription::class)->findAll();
        return $this->render('google/show.html.twig',[
            'listeInscrits'=>$listeInscrits
        ]);
        
    }

    public function deleteInscrit(int $id){
        $em = $this->getDoctrine()->getManager();
        /** @var Inscription $inscrit */
        $inscrit = $em->getRepository(Inscription::class)->findOneBy(array('id'=>$id));
        $em->remove($inscrit);
        $em->flush();
        return $this->redirectToRoute('show');
    }
}
